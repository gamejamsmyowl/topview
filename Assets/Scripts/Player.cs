﻿using UnityEngine;
using System.Collections;
using System;

public class Player : MonoBehaviour {

    public AudioClip attackSound1;
    public AudioClip attackSound2;
    public AudioSource audio;

    public Animator playerAnimator;

    private Movement m_movement;
    private Attack m_attack;
    private Vector2 m_input;

    private bool m_facingRight;
    private bool m_firstAttackAnim;

    void Start()
    {
        m_movement = GetComponent<Movement>();
        m_attack = GetComponent<Attack>();
    }

    void Update()
    {
        m_input.x = Input.GetAxis("Horizontal");
        m_input.y = Input.GetAxis("Vertical");

        playerAnimator.SetFloat("Speed", Mathf.Abs(m_input.x));

        if (Input.GetMouseButtonDown(0))
        {
            m_firstAttackAnim = !m_firstAttackAnim;

            if (m_firstAttackAnim)
            {
                playerAnimator.SetTrigger("Attack1");
            }

            else
            {
                playerAnimator.SetTrigger("Attack2");
            }


            Invoke("CallAttack", 0.4f);
        }
    }

    private void CallAttack()
    {
        m_attack.PrimaryAttack(onFinishAttack);
    }

    private void onFinishAttack()
    {
        
    }

    void FixedUpdate()
    {
        m_movement.Move(m_input);
    }
}
