﻿using UnityEngine;
using System.Collections;
using System;

public class Attack : MonoBehaviour {

    public float damage;
    public float attackDuration;
    public float cooldownTime;
    public Collider2D damageArea;

    private float m_time;
    private bool m_onCooldown;

    private Action m_onFinishAttack;

    void Start()
    {
        damageArea.enabled = false;
    }

    void Update()
    {
        if (m_onCooldown)
        {
            m_time += Time.deltaTime;

            if (m_time > cooldownTime)
            {
                m_onCooldown = false;
                m_time = 0;
            }
        }
    }

    public void PrimaryAttack(Action onAttackFinish)
    {
        if (m_onCooldown) return;

        damageArea.enabled = true;
        m_onCooldown = true;

        m_onFinishAttack = onAttackFinish;
        Invoke("InvokeAttackCallback", attackDuration);
    }

    private void InvokeAttackCallback()
    {
        damageArea.enabled = false;
        m_onFinishAttack.Invoke();
    }

}
