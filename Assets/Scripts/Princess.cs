﻿using UnityEngine;
using System.Collections;

public class Princess : MonoBehaviour {

    public Animator anim;

    public bool IsTaken { get; set; }

    void Update()
    {
        anim.SetBool("IsTake", IsTaken);
    }
}
